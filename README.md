# meet 4 - blog

<h2>Tugas Meet 4<h2>

<p>Membuat table dengan migration</p>
<p>berikut link ERD nya:</p>
<p>https://pandhuwibowo.medium.com/part-6-laravel-migrations-2b6acf7e4b53</p>

***
***

<h2> Tugas Project Akhir </h2>

Membuat sebuah program menggunakan laravel dengan menerapkan materi yang sudah diajarkan dengan minimal kriteria diantaranya terdapat CRUD, login, register dan level akses.

Ketentuan:
- Topik: bebas
- Waktu: sampai pertemuan akhir
- 1 kelompok 3 orang
- Tidak boleh menggunakan template
- Proses pengerjaan harus menggunakan git: Bikin repositori di grup himtidev web gitlab
- Deploy ke: (Heroku dan db4free) atau https://www.000webhost.com/
- Dipresentasikan (black box & white box)
